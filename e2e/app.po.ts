import { browser, element, by } from 'protractor';

export class SmartumTaskPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('smrt-root h1')).getText();
  }
}
