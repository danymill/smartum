import { SmartumTaskPage } from './app.po';

describe('smartum-task App', function() {
  let page: SmartumTaskPage;

  beforeEach(() => {
    page = new SmartumTaskPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('smrt works!');
  });
});
