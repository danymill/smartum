import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'smrt-pagenotfounnd',
  templateUrl: 'pagenotfound.component.html',
  styleUrls: ['pagenotfound.component.scss']
})
export class PagenotfoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
