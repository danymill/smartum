import { Injectable } from '@angular/core';

import { User } from "./user";

@Injectable()
export class UserService {
  private users: User[] = [
    new User('bla@bla.bla', 'Anyname', 'Anonym', 'USA', 'Anycompany', 'type1', '2016.01.02', 80, true, 555),
    new User('bla1@bla.bla', 'Anyname1', 'Anonym2', 'USSR', 'Anycompany', 'type1', '2016.01.02', 10, true, 455),
    new User('bla2@bla.bla', 'Anyname2', 'Anonym3', 'USSR', 'Anycompany', 'type2', '2016.03.05', 19, true, 655),
    new User('bla3@bla.bla', 'Anyname3', 'Anonym4', 'GDR', 'Anycompany', 'type0', '2016.12.07', 5, true, 675),
    new User('bla4@bla.bla', 'Anyname4', 'Anonym6', 'Egypt', 'Anycompany', 'type3', '2016.11.22', 67, true, 523),
    new User('bla5@bla.bla', 'Anyname5', 'Anonym7', 'USA', 'Anycompany', 'type1', '2016.07.24', 32, true, 234),
    new User('bla6@bla.bla', 'Anyname6', 'Anonym8', 'China', 'Anycompany', 'type3', '2016.03.12', 67, true, 546)
  ];

  constructor() { }

  getUsers() {
    return this.users;
  }

  editUser(user: User) {

  }
}
