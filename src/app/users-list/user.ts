export class User {
  constructor(
    public email: string,
    public firstName: string,
    public lastName: string,
    public country: string,
    public company: string,
    public subsType: string,
    public subsDate: string,
    public level: number,
    public validated: boolean,
    public coins: number
  ) {}
}
