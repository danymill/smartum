import { Component, OnInit } from '@angular/core';

import { UserService } from "./user.service";
import {User} from "./user";

@Component({
  selector: 'smrt-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  users: User[] = [];

  constructor(private UserServise: UserService) { }

  ngOnInit() {
    this.users = this.UserServise.getUsers();
  }

  onLoadMore() {
    console.log('Here will be load more functionality...');
  }
}
