import { NgModule } from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

import { UsersListComponent } from "./users-list/users-list.component";
import { PagenotfoundComponent } from "./pagenotfound/pagenotfound.component";

const APP_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'users-list',
    pathMatch: 'full'
  },
  {
    path: 'users-list',
    component: UsersListComponent
  },
  {
    path: '**',
    component: PagenotfoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(APP_ROUTES)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {}